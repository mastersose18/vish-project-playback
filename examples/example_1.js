var fromProjection = "+proj=utm +zone=32 +ellps=GRS80 +units=m +no_defs ";
var toProjection = "+proj=longlat +ellps=WGS84 +datum=WGS84 ";

var demoTracks = [];

$(function () {

    // Setup leaflet map
    var map = new L.Map('map');

    // L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    //     attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    // }).addTo(map);

    // Center map and default zoom level
    map.setView([0, 0], 1);

    // Add GeoJSON Niedersachsen
    $.getJSON( "/data/niedersachsen_simplify200.geojson", function( data ) {
        console.log(data);
        var ndsLayer = L.geoJson(data, {
            style: function (feature) {
                return { 
                    fillColor: 'rgba(0,0,0,0)',
                    weight: 2,
                    opacity: 1,
                    color: 'yellow',
                    dashArray: '3',
                    fillOpacity: 0.7
                 };
            }
        });
        ndsLayer.addTo(map);
        map.fitBounds(ndsLayer.getBounds());
    });

    var flightTracks = [];
    
    // Add Flights to Map
    $.getJSON("/flights.json", function (data) {

        var count = data.map(flight => flight.images.length).reduce((a, c) => a + c);
        console.log(count);

        for (key in data.slice(0,10)) {
            flight = data[key];

            flight.images
                .sort((a, b) => a.id - b.id)
                .map((a) => {
                    a.geometry.coordinates = proj4(fromProjection, toProjection, a.geometry.coordinates);
                });
            
            countrySwitch = Math.random();
            
            template = {
                "type": "Feature",
                "geometry": {
                    "type": "MultiPoint",
                    "coordinates": [

                    ]
                },
                "properties": {
                    "id": flight.id,
                    "country": countrySwitch < .5 ? 'us' : 'uk' ,
                    "path_options": {
                        "color":  countrySwitch < .5 ? 'blue' : 'red' 
                    },
                    "time": [
                    ],
                }
            };
                
            tracks = [];

            for (key in flight.images) {
                c = flight.images[key].geometry.coordinates;
                // L.marker([c[1], c[0]]).addTo(map)
                var track = [c[1], c[0]];
                // track.push(c[1]); track.push(c[0])
                // tracks.push(track);

                template.geometry.coordinates.push(c);
                t = new Date(flight.date);

                secs = ((24 * 60 * 60) / flight.images.length) * key;
                t.setSeconds(t.getSeconds() + secs);
                template.properties.time.push(t.getTime());

            }

            
            demoTracks.push(template);
            data[key] = flight;

            console.log("next flight");
        }

        console.log(demoTracks);

    // Get start/end times
    var startTime = new Date(demoTracks[0].properties.time[0]);
    var endTime = new Date('1945-01-01');

    // Create a DataSet with data
    // var timelineData = new vis.DataSet([{ start: startTime, end: endTime, content: 'Demo GPS Tracks' }]);
        
        var timelineData = new vis.DataSet({});
        
        for (flight of demoTracks) {
            console.log(flight);
            timelineData.add({
                start: flight.properties.time[0],
                end: flight.properties.time[flight.properties.time.length - 1],
                content: flight.properties.id,
                className: flight.properties.country
            });
        }

    // Set timeline options
    var timelineOptions = {
      "width":  "100%",
      "height": "300px",
      "style": "box",
      "axisOnTop": true,
      "showCustomTime":true
    };
    
    // Setup timeline
    var timeline = new vis.Timeline(document.getElementById('timeline'), timelineData, timelineOptions);
        
    // Set custom time marker (blue)
        timeline.setCustomTime(startTime);
        
        timeline.on('select', (items) => {
            console.log(items);
        })

        timeline.on('mouseOver', (event) => {
            console.log(event);
        })

    // =====================================================
    // =============== Playback ============================
    // =====================================================
    
    // Playback options
    var playbackOptions = {

        playControl: true,
        dateControl: true,
        speed: 5.0,
        maxInterpolationTime: 365 * 24 * 60 * 60 * 1000,
        fadeMarkersWhenStale: true,
        // layer and marker options
        layer : {
            pointToLayer : function(featureData, latlng) {
                var result = {};
                
                if (featureData && featureData.properties && featureData.properties.path_options) {
                    result = featureData.properties.path_options;
                }
                
                if (!result.radius){
                    result.radius = 5;
                }
                
                return new L.CircleMarker(latlng, result);
            }
        },
        
        marker: { 
            icon: L.icon({
                iconUrl: '../plane-usaf.png',
                iconSize: [50, 50],
                iconAnchor: [25, 50]
            }),
            getPopup: function(featureData) {
                var result = '';
                
                if (featureData && featureData.properties && featureData.properties.title) {
                    result = featureData.properties.title;
                }
                
                return result;
            }
        },
        orientIcons : true
    };
        
    // Initialize playback
    var playback = new L.Playback(map, null, onPlaybackTimeChange, playbackOptions);
    
        playback.setData(demoTracks);    
        playback.setSpeed(60*60);

    // Uncomment to test data reset;
    //playback.setData(blueMountain);    
    
    // Set timeline time change event, so cursor is set after moving custom time (blue)
    timeline.on('timechange', onCustomTimeChange);    

    // A callback so timeline is set after changing playback time
    function onPlaybackTimeChange (ms) {
        timeline.setCustomTime(new Date(ms));
    };
    
    // 
    function onCustomTimeChange(properties) {
        if (!playback.isPlaying()) {
            playback.setCursor(properties.time.getTime());
        }        
        }   
    
    });    
});
